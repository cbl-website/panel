import { observable } from 'mobx';

export enum EventStatus {
  AwaitingDecision,
  Rejected,
  Accepted,
};

export interface EventProps {
  id: string;
  eventName: string;
  submitterName: string;
  submitterEmail: string;
  description: string;
  startDate: Date;
  endDate: Date;
  status: EventStatus;
}

export class Event {
  @observable
  public id: string;

  @observable
  public eventName: string;

  @observable
  public submitterName: string;

  @observable
  public submitterEmail: string;

  @observable
  public description: string;

  @observable
  public startDate: Date;

  @observable
  public endDate: Date;

  @observable
  public status: EventStatus;

  public constructor(props: EventProps) {
    this.id = props.id;
    this.eventName = props.eventName;
    this.submitterName = props.submitterName;
    this.submitterEmail = props.submitterEmail;
    this.description = props.description;
    this.startDate = props.startDate;
    this.endDate = props.endDate;
    this.status = props.status;
  }
}
