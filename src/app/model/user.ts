import { observable } from 'mobx';

export class User {
  @observable
  public id: string;

  @observable
  public username: string;

  @observable
  public email: string;
}
