import * as React from 'react';
import * as style from './style.css';

export const Card: React.FC = ({ children }) => {
  return (
    <div className={style.card}>
      <div className={style.cardContent}>
        {children}
      </div>
    </div>
  );
}
