import React, { useState, useContext } from 'react';
import { Event, EventStatus } from 'app/model';
import { format } from 'date-fns';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CSSTransition } from 'react-transition-group';
import { observer } from 'mobx-react';
import EventStore from 'app/stores/eventStore';
import * as style from './style.css';
import * as classnames from 'classnames/bind';

const cx = classnames.bind(style);

export interface EventListingProps {
  event: Event;
}

export const EventListing = observer((props: EventListingProps) => {
  const [active, setActive] = useState(false);
  const eventStore = useContext(EventStore);

  const eventListingClassnames = cx('eventListing', { active });
  const chevronClassnames = cx('chevron', { active });

  const handleAccept = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
  }

  const handleDeny = (e: React.MouseEvent<HTMLButtonElement>) => {
    e.stopPropagation();
  }

  return (
    <div className={eventListingClassnames}
      onClick={() => setActive(!active)}
      onMouseEnter={() => eventStore.selectedEventId = props.event.id}
      onMouseLeave={() => eventStore.selectedEventId = undefined}
    >
      <div className={style.eventContainer}>
        <FontAwesomeIcon className={chevronClassnames} icon="chevron-down" />
        <div className={style.eventContent}>
          <h4>{props.event.eventName}</h4>
          <div className={style.eventInfo}>
            <div className={style.verticalGroup}>
              <span className={style.subtitle}>{props.event.submitterName}</span>
              <span className={style.subtitle}>{props.event.submitterEmail}</span>
            </div>
            <div className={style.dateGroup}>
              <span>From:</span>
              <span className={style.date}>{format(props.event.startDate, 'yyyy-MM-dd HH:mm:ss')}</span>
              <span>To:</span>
              <span className={style.date}>{format(props.event.endDate, 'yyyy-MM-dd HH:mm:ss')}</span>
            </div>
          </div>
        </div>
        <div className={cx('buttonGroups', { active: props.event.status === EventStatus.AwaitingDecision })}>
          <button className={style.accept} onClick={(e) => handleAccept(e)}>
            <FontAwesomeIcon icon="check" />
          </button>
          <button className={style.deny} onClick={(e) => handleDeny(e)}>
            <FontAwesomeIcon icon="times" />
          </button>
        </div>
      </div>
      <CSSTransition
        in={active}
        timeout={400}
        unmountOnExit
        classNames={{
          enterActive: style.eventEnterActive,
          enterDone: style.eventEnterDone,
          exitActive: style.eventExitActive,
          exitDone: style.eventExitDone,
        }}
      >
        <span className={style.description}>{props.event.description}</span>
      </CSSTransition>
    </div>
  );
});
