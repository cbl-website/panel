import React from 'react';
import * as style from './style.css';
import { Header } from '../Header';

export const Layout: React.FC = ({ children }) => {
  return (
    <div className={style.container}>
      <Header />
      {children}
    </div>
  )
};
