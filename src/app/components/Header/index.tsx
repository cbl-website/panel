import React from 'react';
import SnowEmoji from 'assets/images/snow.svg';
import PeachEmoji from 'assets/images/peach.svg';
import FireEmoji from 'assets/images/fire.svg';
import { Link } from 'react-router-dom';
import * as style from './style.css';

export const Header = () => (
  <header className={style.header}>
    <Link className={style.logo} to="/">
      <SnowEmoji />
      <PeachEmoji />
      <FireEmoji />
    </Link>
    <span>Panel</span>
    <nav>
      <Link to="/events">Event Scheduling</Link>
      <Link to="/users">User Management</Link>
    </nav>
  </header>
);
