import React, { useRef, useEffect, useState } from 'react';
import ReactDom from 'react-dom';
import { Card } from 'app/components/Card';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { CSSTransition } from 'react-transition-group';
import * as style from './style.css';


export type FadeType = 'in' | 'out';
export interface ModalProps {
  id: string;
  isOpen: boolean;
  onClose: () => void;
}

const modalRoot = document.getElementById('modal-root');

export const Modal: React.FC<ModalProps> = (props) => {
  const [fade, setFade] = useState(false);
  const background = useRef<HTMLDivElement>();

  useEffect(() => handleComponentMount(), []);

  const handleComponentMount = () => {
    window.addEventListener('keydown', (e) => handleEscKeyDown(e), false);
    setFade(true);
  }

  const handleComponentUnmount = () => {
    window.removeEventListener('keydown', (e) => handleEscKeyDown(e), false);
    props.onClose();
  }

  const handleEscKeyDown = (e: KeyboardEvent) => {
    if (e.key !== 'Escape') {
      return;
    }

    setFade(false);
  }

  return ReactDom.createPortal(
    <CSSTransition
      in={fade}
      exit={true}
      timeout={500}
      classNames={{
        enterActive: style.enterActive,
        enterDone: style.enterDone,
        exitActive: style.exitActive,
        exitDone: style.exitDone,
      }}
      onExited={() => handleComponentUnmount()}
    >
      <div
        id={props.id}
        className={style.modal}
        role="dialog"
      >
        <div className={style.modalContainer}>
          <Card>
            <button className={style.close} onClick={() => setFade(false)}>
              <FontAwesomeIcon icon="times" />
            </button>
            {props.children}
          </Card>
        </div>
        <div
          className={style.background}
          onMouseDown={() => setFade(false)}
          ref={background}
        />
      </div>
    </CSSTransition>,
    modalRoot,
  );
}
