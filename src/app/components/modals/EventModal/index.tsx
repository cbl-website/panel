import React, { useRef, useEffect, useState } from 'react';
import { Modal } from '../Modal';
import * as style from './style.css';

export interface EventModalProps {
  isOpen: boolean;
  onClose: () => void;
}

export const EventModal: React.FC<EventModalProps> = (props) => {
  return (
    props.isOpen && (
      <Modal id="eventModal" isOpen={props.isOpen} onClose={() => props.onClose()}>
        <div className={style.content}>

        </div>
      </Modal>
    )
  );
}
