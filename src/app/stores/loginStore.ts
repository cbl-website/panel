import { useLocalStore } from 'mobx-react';

export type LoginStore = ReturnType<typeof useLoginStore>;
export const useLoginStore = () => {
  const store = useLocalStore(() => ({
    usernameOrEmail: '',
    password: '',
    authenticating: false,
    authenticated: false,
    authResponse: '',

    beginAuthentication: () => {
      store.authenticating = true;
      setTimeout(() => {
        store.authenticating = false;
        store.authenticated = true;
        store.authResponse = 'Invalid credentials.';
      }, 1000);
    },
  }));

  return store;
};
