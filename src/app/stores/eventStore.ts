import { Event } from 'app/model';
import { observable } from 'mobx';
import { createContext } from 'react';


export class EventStoreModel {
  @observable
  public events: Array<Event>;

  @observable
  public selectedEventId: string;

  public constructor() {
    this.events = [];
  }
}

export default createContext(new EventStoreModel());
