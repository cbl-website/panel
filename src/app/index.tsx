import * as React from 'react';
import { hot } from 'react-hot-loader/root';
import { Router, Switch } from 'react-router';
import { ProtectedRoute, AuthenticatedRoute } from 'app/util';
import { HomePage } from 'app/containers/HomePage';
import { LoginPage } from 'app/containers/LoginPage';
import { History } from 'history';
import { Route, Redirect } from 'react-router-dom';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faTimes, faCheck, faChevronDown, faSyncAlt, faPlus } from '@fortawesome/free-solid-svg-icons';

library.add(faCheck, faTimes, faChevronDown, faSyncAlt, faPlus);

export interface AppProps {
  history: History<any>;
}

export const Application = hot(({ history }) => {
  return true !== undefined && (
    <Router history={history}>
      <Switch>
        <Redirect exact from="/" to="/login" />
        <Route path="/login" component={LoginPage} />
        <Route path="/dashboard" component={HomePage} />
      </Switch>
    </Router>
  );
});
