import React, { useRef, useState, useContext } from 'react';
import { Event, EventStatus } from 'app/model';
import { Layout } from 'app/components/Layout';
import { observer } from 'mobx-react';
import { EventListing } from 'app/components/EventListing';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import EventStore from 'app/stores/eventStore';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import '!style-loader!css-loader!@fullcalendar/core/main.css';
import '!style-loader!css-loader!@fullcalendar/daygrid/main.css';
import '!style-loader!css-loader!./calendar.css';
import * as classnames from 'classnames/bind';
import * as style from './style.css';
import { EventModal } from 'app/components/modals/EventModal';

const cx = classnames.bind(style);

const exampleEvents = [
  new Event({
    id: '1',
    eventName: 'The fucking chevron quest.',
    submitterName: 'Kamilczak020',
    submitterEmail: 'kamilczak020@gmail.com',
    description: 'This is a quest to make chevrons behave correctly. I have no idea why they be so fucky but they do be.',
    startDate: new Date('2020-07-01 20:30'),
    endDate: new Date('2020-07-04 20:30'),
    status: EventStatus.AwaitingDecision,
  }),
  new Event({
    id: '2',
    eventName: 'This is an example event.',
    submitterName: 'Greenchill',
    submitterEmail: 'kamil.solecki@toptal.com',
    description: 'This event is about playing music by Max Cooper, on his quest for Glass.',
    startDate: new Date('2020-07-03 14:30'),
    endDate: new Date('2020-07-04 18:30'),
    status: EventStatus.AwaitingDecision,
  }),
]

export const renderEvents = (tab: EventStatus) => {
  return exampleEvents
    .filter((event) => event.status === tab)
    .map((event) => <EventListing event={event} />)
}

export const HomePage = observer(() => {
  const [eventModalOpen, setEventModalOpen] = useState(false);
  const [tab, setTab] = useState(EventStatus.AwaitingDecision);
  const eventStore = useContext(EventStore);
  const calendar = useRef<FullCalendar>(null);

  return (
    <Layout>
      <div className={style.pageContainer}>
        <div className={style.sidebar}>
          <h1>Events submitted for review</h1>
          <div className={style.tabSelect}>
            <button
              className={cx({ active: tab === EventStatus.AwaitingDecision })}
              onClick={() => setTab(EventStatus.AwaitingDecision)}
            >
              Awaiting Decision
            </button>
            <button
              className={cx({ active: tab === EventStatus.Accepted })}
              onClick={() => setTab(EventStatus.Accepted)}
            >
              Accepted
            </button>
            <button
              className={cx({ active: tab === EventStatus.Rejected })}
              onClick={() => setTab(EventStatus.Rejected)}
            >
              Rejected
            </button>
            <button
              className={style.add}
              onClick={() => setEventModalOpen(true)}
            >
              <FontAwesomeIcon icon="plus" />
            </button>
            <button className={style.refresh}>
              <FontAwesomeIcon icon="sync-alt" />
            </button>
          </div>
          {renderEvents(tab)}
        </div>
        <div className={style.calendarContainer}>
          <FullCalendar
            ref={calendar}
            defaultView="dayGridMonth"
            plugins={[dayGridPlugin, interactionPlugin]}
            header={{
              left: 'dayGridMonth',
              right: 'prevYear,prev,next,nextYear',
            }}
            events={
              exampleEvents.map((event) => ({
                color: eventStore.selectedEventId === event.id ? '#feae01' : '#1d2b4e',
                title: event.eventName,
                start: event.startDate,
                end: event.endDate,
              }))
            }
          />
        </div>
      </div>
      <EventModal isOpen={eventModalOpen} onClose={() => setEventModalOpen(false)} />
    </Layout>
  );
});
