import React, { useState } from 'react';
import SnowEmoji from 'assets/images/snow.svg';
import PeachEmoji from 'assets/images/peach.svg';
import FireEmoji from 'assets/images/fire.svg';
import { observer } from 'mobx-react';
import { CSSTransition } from 'react-transition-group';
import { useLoginStore } from 'app/stores';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { useHistory } from 'react-router';
import { exists } from 'app/util';
import * as classnames from 'classnames/bind';
import * as style from './style.css';

const cx = classnames.bind(style);

export const LoginPage = observer(() => {
  const history = useHistory();
  const loginStore = useLoginStore();
  const [isSubmittingCycle, setSubmittingCycle] = useState(false);
  const [isLoadingCycle, setLoadingCycle] = useState(false);
  const [isResponseCycle, setResponseCycle] = useState(false);
  const [isRedirectCycle, setRedirectCycle] = useState(false);
  const [isResponseCycleQuit, setResponseCycleQuit] = useState(false);
  const [isSubmittingCycleQuit, setSubmittingCycleQuit] = useState(false);
  const [isValidationErrorActive, setValidationErrorActive] = useState(false);

  const handleUsernameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    loginStore.usernameOrEmail = e.target.value;
    setValidationErrorActive(false);
  }

  const handlePasswordChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    loginStore.password = e.target.value;
    setValidationErrorActive(false);
  }

  const renderLoginButtonContent = () => {
    if (!isSubmittingCycle && !isSubmittingCycleQuit) {
      return (<span>Log In</span>);
    } else {
      if (isResponseCycle || isResponseCycleQuit) {
        if (!loginStore.authenticating && !loginStore.authenticated) {
          return (<FontAwesomeIcon className={style.text} icon="times" />);
        }

        if (!loginStore.authenticating && loginStore.authenticated) {
          return (<FontAwesomeIcon className={style.text} icon="check" />);
        }
      }
    }
  }

  const beginSubmit = (e?: React.FormEvent<HTMLFormElement>) => {
    if (exists(e)) {
      e.preventDefault();
    }

    if (!isLoadingCycle) {
      setSubmittingCycle(true);
    }
  }

  const beginAuthentication = () => {
    setLoadingCycle(true);
    loginStore.beginAuthentication();
  }

  const checkAuthenticationState = () => {
    setLoadingCycle(false);
    if (loginStore.authenticating) {
      setTimeout(() => setLoadingCycle(true), 500);
    } else {
      beginResponse();
    }
  }

  const beginResponse = () => {
    setResponseCycle(true);

    if (!loginStore.authenticating && !loginStore.authenticated) {
      setValidationErrorActive(true);
    }
  }

  const finishResponse = () => {
    if (!loginStore.authenticating && !loginStore.authenticated) {
      setTimeout(() => {
        setResponseCycle(false);
        setResponseCycleQuit(true);
      }, 1000);

      setTimeout(() => {
        setResponseCycleQuit(false);
        setSubmittingCycle(false);
        setSubmittingCycleQuit(true);
      }, 1400);
    }

    if (!loginStore.authenticating && loginStore.authenticated) {
      setRedirectCycle(true);
    }
  }

  const submitButtonClassNames = cx('submitButton', {
    success: isResponseCycle && !loginStore.authenticating && loginStore.authenticated,
    failure: isResponseCycle && !loginStore.authenticating && !loginStore.authenticated,
  });

  const inputClassnames = cx({ failure: isValidationErrorActive });

  const errorLabelClassnames = cx('errorLabel', {
    active: isValidationErrorActive,
  });

  return (
    <div className={style.container}>
      <div className={style.loginContainer}>
        <div className={style.logo}>
          <SnowEmoji />
          <PeachEmoji />
          <FireEmoji />
        </div>
        <span>Chill But Lit Panel</span>
        <form onSubmit={(e) => beginSubmit(e)}>
          <input
            type="text"
            placeholder="Username or email"
            className={inputClassnames}
            onChange={(e) => handleUsernameChange(e)}
          />
          <input
            type="password"
            placeholder="Password"
            className={inputClassnames}
            onChange={(e) => handlePasswordChange(e)}
          />
          <span className={errorLabelClassnames}>{loginStore.authResponse}</span>
          <CSSTransition
            in={isSubmittingCycle}
            exit={true}
            timeout={1000}
            classNames={{
              enterActive: style.loadingEnterActive,
              enterDone: style.loadingEnterDone,
              exitActive: style.loadingExitActive,
              exitDone: style.loadingExitDone,
            }}
            onEntered={() => beginAuthentication()}
            onExited={() => setSubmittingCycleQuit(false)}
          >
            <CSSTransition
              in={isLoadingCycle}
              timeout={1800}
              classNames={{
                enterActive: style.authenticatingEnterActive,
                enterDone: style.authenticatingEnterDone,
              }}
              onEntered={() => checkAuthenticationState()}
            >
              <CSSTransition
                in={isResponseCycle}
                exit={true}
                timeout={400}
                classNames={{
                  enter: style.responseEnter,
                  enterActive: style.responseEnterActive,
                  enterDone: style.responseEnterDone,
                  exit: style.responseExit,
                  exitActive: style.responseExitActive,
                  exitDone: style.responseExitDone,
                }}
                onEntered={() => finishResponse()}
              >
                <CSSTransition
                  in={isRedirectCycle}
                  timeout={2000}
                  classNames={{
                    enterActive: style.redirectEnterActive,
                    enterDone: style.redirectEnterDone,
                  }}
                // onEntered={() => history.push('/dashboard')}
                >
                  <div className={style.submitContainer}>
                    <div className={style.submitLoader}>
                      <div className={cx('hold', 'left')}>
                        <div className={style.fill} />
                      </div>
                      <div className={cx('hold', 'right')}>
                        <div className={style.fill} />
                      </div>
                    </div>
                    <div className={submitButtonClassNames} onClick={() => beginSubmit()}>
                      {renderLoginButtonContent()}
                    </div>
                  </div>
                </CSSTransition>
              </CSSTransition>
            </CSSTransition>
          </CSSTransition>
        </form>
      </div>
    </div>
  );
});
